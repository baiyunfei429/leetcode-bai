/**
 * 53. 最大子序和【中等】
 * 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

示例 1：
输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。

示例 2：
输入：nums = [1]
输出：1

示例 3：
输入：nums = [0]
输出：0

示例 4：
输入：nums = [-1]
输出：-1

示例 5：
输入：nums = [-100000]
输出：-100000
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
// 暴力解法
var maxSubArray = function(nums) {
  
};

// 贪心算法
var maxSubArray2 = function(nums) {
  let res = Number.MIN_VALUE;
  let count = 0;
  for (let i = 0; i < nums.length; i++) {
    count += nums[i];
    if (count > res) {
      res = count;
    }
    if (count <= 0) count = 0;
  }
  return res;
}

// 动态规划
var maxSubArray3 = function(nums) {
  const len = nums.length;
  let pre = 0, maxAns = nums[0];
  for (let i = 0; i < len; i++) {
    pre = Math.max(pre + nums[i], nums[i]);
    maxAns = Math.max(maxAns, pre);
  }
  return maxAns;
}


const demo1 = [-2,1,-3,4,-1,2,1,-5,4]
const res1 = maxSubArray2(demo1);
console.log(res1)