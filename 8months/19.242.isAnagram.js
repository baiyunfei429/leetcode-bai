/**
 * 242. 有效的字母异位词 【简单】
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。

注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。

示例 1:
输入: s = "anagram", t = "nagaram"
输出: true

示例 2:
输入: s = "rat", t = "car"
输出: false
*/

/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function(s, t) {
  return s.length == t.length && [...s].sort().join('') === [...t].sort().join('')
};
var isAnagram2 = function(s, t) {
  if (s.length !== t.length) return false;
  const resSet = new Array(26).fill(0);
  const base = 'a'.charCodeAt();
  console.log(base)
  for (const i of s) {
    resSet[i.charCodeAt() - base]++;
  }
  for (const i of t) {
    if (!resSet[i.charCodeAt() - base]) return false;
    resSet[i.charCodeAt() - base]--;
  }
  return true;
};
const s = "anagram", t = "nagaram";
console.time('start');
const res1 = isAnagram2(s, t);
console.timeEnd('start');
console.log(res1);