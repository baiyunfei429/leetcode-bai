/**
 * 27. 移除元素 【简单】
 * 你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 *
示例 1：

输入：nums = [3,2,2,3], val = 3
输出：2, nums = [2,2]
解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。你不需要考虑数组中超出新长度后面的元素。例如，函数返回的新长度为 2 ，而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。
示例 2：

输入：nums = [0,1,2,2,3,0,4,2], val = 2
输出：5, nums = [0,1,4,0,3]
解释：函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。注意这五个元素可为任意顺序。你不需要考虑数组中超出新长度后面的元素。
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
var removeElement = function(nums, val) {
  let size = nums.length;
  for (let i = 0; i < size; i++) {
    if (nums[i] === val) {
      for (let j = i + 1; j < size; j++) {
        nums[j - 1] = nums[j]
      }
      i--;
      size--;
    }
  }
  console.log('nums: ', nums)
  return size
};

// const res1 = removeElement([0,1,2,2,3,0,4,2], 2);
// console.log(res1)

var removeElement2 = function(nums, val) {
  let left = 0, right = nums.length - 1;
  while(left <= right) {
    if (nums[left] === val) {
      nums[left] = nums[right];
      right--;
    } else {
      left++;
    }
  }
  return left;
};

// const res2 = removeElement2([0,1,2,2,3,0,4,2,1], 2);
// console.log(res2)
/*
left = 2时，[0,1,1,2,3,0,4,2,1]
left = 3时，[0,1,1,2,3,0,4,2,1]
left = 4时，[0,1,1,4,3,0,4,2,1]
*/

var removeElement3 = function(nums, val) {
  const len = nums.length;
  console.log(len)
  let slow = 0, fast = 0;
  while(fast < len) {
    if (nums[fast] !== val) {
      nums[slow] = nums[fast];
      slow++;
    }
    fast++
  }
  return slow;
};

const res3 = removeElement3([0,1,2,2,3,0,4,2,1], 2);
console.log(res3)
/*
init: len = 9, fast = 0, slow = 0;

fast = 0, [0,1,2,2,3,0,4,2,1],slow = 1,fast = 1
fast = 1, [0,1,2,2,3,0,4,2,1],slow = 2, fast = 2
fast = 2, [0,1,2,2,3,0,4,2,1],slow不变，=2，fast = 3
fast = 3, [0,1,2,2,3,0,4,2,1],slow不变，=2，fast = 4
fast = 4, [0,1,3,2,3,0,4,2,1],slow=3，fast = 5
fast = 5, [0,1,3,0,3,0,4,2,1],slow=4，fast = 6
fast = 6, [0,1,3,0,4,0,4,2,1],slow=5，fast = 7
fast = 7, [0,1,3,0,4,0,4,2,1],slow不变，=5，fast = 8
fast = 8, [0,1,3,0,4,1,4,2,1],slow=6，fast = 9，结束
*/