/**
 * 977. 有序数组的平方 【简单】
 * 给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。

示例 1：

输入：nums = [-4,-1,0,3,10]
输出：[0,1,9,16,100]
解释：平方后，数组变为 [16,1,0,9,100]
排序后，数组变为 [0,1,9,16,100]
示例 2：

输入：nums = [-7,-3,2,3,11]
输出：[4,9,9,49,121]
*/

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares = function(nums) {
  const len = nums.length;
  let left = 0; right = len - 1;
  const res = []
  while(left <= right) {
    const leftVal = nums[left] * nums[left];
    const rightVal = nums[right] * nums[right];
    if (leftVal > rightVal) {
      res.unshift(leftVal);
      left++;
    } else {
      res.unshift(rightVal);
      right--;
    }
  }
  return res;
};

const demo1 = [-4,-1,0,3,10];
console.time('start')
const res1 = sortedSquares(demo1);
console.timeEnd('start')
console.log(res1)