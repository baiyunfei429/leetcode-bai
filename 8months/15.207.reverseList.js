/**
 * 206. 反转链表 【简单】
 * 给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
 * 
输入：head = [1,2,3,4,5]
输出：[5,4,3,2,1]
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
// 方法1：迭代方法
var reverseList = function(head) {
  if (!head || !head.next) return head;
  let pre = null, cur = head;
  while (cur) {
    const temp = cur.next;
    cur.next = pre;
    pre = cur;
    cur = temp; // 最后一个节点时，cur.next已经等于null了，所以不能返回cur，得返回pre
  }
  return pre;
};

// 方法2：递归解决
var reverseList2 = function(head) {
  if (head == null || head.next == null) return head;
  const newHead = reverseList2(head.next);
  head.next.next = head;
  head.next = null;
  return newHead;
};

// 方法3：使用虚拟头结点解决链表翻转
// 使用虚拟头结点，通过头插法实现链表的翻转（不需要栈）
// 迭代方法：增加虚头结点，使用头插法实现链表翻转
var reverseList2 = function(head) {
    // 创建虚头结点
    dumpyHead = new ListNode(-1);
    dumpyHead.next = null;
    // 遍历所有节点
    cur = head;
    while(cur != null){
        temp = cur.next;
        // 头插法
        cur.next = dumpyHead.next;
        dumpyHead.next = cur;
        cur = temp;
    }
    return dumpyHead.next;
}

// 方法4：使用栈解决反转链表的问题
// 首先将所有的结点入栈
// 然后创建一个虚拟虚拟头结点，让cur指向虚拟头结点。然后开始循环出栈，每出来一个元素，就把它加入到以虚拟头结点为头结点的链表当中，最后返回即可。
/*
var reverseList = function(head) {
    // 如果链表为空，则返回空
    if (head == null) return null;
    // 如果链表中只有只有一个元素，则直接返回
    if (head.next == null) return head;
    // 创建栈 每一个结点都入栈
    Stack<ListNode> stack = new Stack<>();
    cur = head;
    while (cur != null) {
        stack.push(cur);
        cur = cur.next;
    }
    // 创建一个虚拟头结点
    pHead = new ListNode(0);
    cur = pHead;
    while (!stack.isEmpty()) {
        node = stack.pop();
        cur.next = node;
        cur = cur.next;
    }
    // 最后一个元素的next要赋值为空
    cur.next = null;
    return pHead.next;
}
*/
const demo1 = [];
console.time('start');
const res1 = reverseList(demo1);
console.timeEnd('start');
console.log(res1);
