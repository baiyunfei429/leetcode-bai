// 求x的n次方
function xn(x, n) {
  let res = 1;
  for (let i = 0; i < n; ++i) {
    res = res * x;
  }
  return res;
}
// console.log(xn(2,5))

// 递归算法
function xn_2(x, n) {
  if (n === 1) {
    return x;
  }
  return xn_2(x, n - 1) * x
}
// console.log(xn_2(2,5))

// 递归算法 优化
function xn_3(x, n) {
  n = Math.floor(n);
  if (n == 0) return 1;
  if (n == 1) return x;
  if (n % 2 == 1) {
    return xn_3(x, n/2) * xn_3(x, n/2) * x;
  }
  return xn_3(x, n/2) * xn_3(x, n/2);
}
// console.log('x_n3=', xn_3(2,5))

// 递归算法 优化 - logn
function xn_3(x, n) {
  n = Math.floor(n);
  if (n == 0) return 1;
  if (n == 1) return x;
  const t = xn_3(x, n/2);
  console.count("count：");
  if (n % 2 == 1) {
    return t * t * x;
  }
  return t * t;
}
console.time('start')
console.log('x_n3=', xn_3(2,6))
console.timeEnd('start')
