/**
 * 20. 有效的括号 【简单】
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。

有效字符串需满足：

左括号必须用相同类型的右括号闭合。
左括号必须以正确的顺序闭合。
每个右括号都有一个对应的相同类型的左括号。

示例 1：
输入：s = "()"
输出：true

示例 2：
输入：s = "()[]{}"
输出：true

示例 3：
输入：s = "(]"
输出：false

*/

/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
  const resArr = [];
  const obj = {
    '{': 1,
    '}': -1,
    '[': 2,
    ']': -2,
    '(': 3,
    ')': -3,
  }
  for (let item of s) {
    const len = resArr.length;
    // console.log(resArr)
    if (obj[item] > 0 && obj[item] + obj[resArr[len - 1]] === 0) {
      // console.log('匹配成功，pop', item)
      resArr.pop();
    } else {
      // console.log('匹配失败，push', item)
      resArr.push(item)
    }
  }
  return resArr.length === 0;
};
const demo1 = "()[]{}";
console.time('start');
const res1 = isValid(demo1);
console.timeEnd('start');
console.log(res1);