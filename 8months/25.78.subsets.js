/**
 * 78. 子集 【中等】
 * 给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。

解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。

示例 1：
输入：nums = [1,2,3]
输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]

示例 2：
输入：nums = [0]
输出：[[],[0]]
*/
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
// 首先我们得把我们思路整理一下👇

// - 这题肯定是求树的所有节点！
// - 对这颗树而言，我们可以遍历它的分支，选择其中一个分支，然后继续向下操作，不选这个分支的话，选择另外一个分支又是另外一个情况，所以每次枚举下一个数字的时候,也就是两种选择：选或不选。
// - 可以考虑使用一个index指针来记录**节点**的状态,即当前递归考察的数字`nums[index]`
// - 递归结束的条件： index === nums.length, 这个时候代表考察完所有的数字，把当前的子集加入题解，结束当前递归分支。
// - 每次结束一个分支，即结束递归，需要撤销当前的选择，（从list中删除），回到选择前的状态，做另外一个选择，即不选择当前的数字，往下递归，继续生成子集。

var subsets = function(nums) {
  const res = []
    const dfs = (index, list) => {
      if (index == nums.length) {         // 递归结束条件
        return res.push(list.slice());   // 加入解集,结束当前的递归
      }
      list.push(nums[index])          // 选择这个元素
      dfs(index + 1, list)           // 往下递归
      list.pop()                    // 递归结束，撤销选择
      dfs(index + 1, list)         // 不选这个元素，往下递归
    }
    dfs(0, [])
    return res
};
const demo1 = [1,2,3];
console.time('start');
const res1 = subsets(demo1);
console.timeEnd('start');
console.log(res1);