/**
 * 59. 螺旋矩阵 II【简单】
 * 给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。
 *
示例 1：
输入：n = 3
图片实例：【https://assets.leetcode.com/uploads/2020/11/13/spiraln.jpg】
输出：[[1,2,3],[8,9,4],[7,6,5]]

示例 2：
输入：n = 1
输出：[[1]]
*/

/**
 * @param {number[]} n
 * @return {number}
 */
var generateMatrix = function(n) {
  let num = 1, target = n * n;
  let left = 0, right = n - 1, top = 0, bottom = n -1;
  let matrix = new Array(n).fill(0).map(() =>new Array(n).fill(0))
  while (num <= target) {
    for (let i = left; i <= right; i++) matrix[top][i] = num++;
    top++;
    for (let i = top; i <= bottom; i++) matrix[i][right] = num++;
    right--;
    for (let i = right; i >= left; i--) matrix[bottom][i] = num++;
    bottom--;
    for (let i = bottom; i >= top; i--) matrix[i][left] = num++;
    left++;
  }
  return matrix;
};
const demo1 = 4;
console.time('start');
const res1 = generateMatrix(demo1);
console.timeEnd('start');
console.log(res1);