/*
题目：给你 k 种面值的硬币，面值分别为 c1, c2 ... ck，再给一个总金额 n，问你最少需要几枚硬币凑出这个金额，如果不可能凑出，则回答 -1 。
比如说，k = 3，面值分别为 1，2，5，总金额 n = 11，那么最少需要 3 枚硬币，即 11 = 5 + 5 + 1 。
*/

function coinChange(coinsArr, amount) {
  if (amount === 0) return 0;
  let res = 0;
  console.log(init, 'init')
  let init = Math.max(...coinsArr);
  while (amount > 0) {
    amount -= init;
    res++;
  }
  if (amount === 0) {
    return res;
  } else if (amount < 0) {
    return -1;
  }
}

const coinsArr = [1,2,5];
const amount = 11;
console.log(coinChange(coinsArr, amount))
