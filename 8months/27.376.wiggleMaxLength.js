/**
 * 376. 摆动序列 【中等】
 * 如果连续数字之间的差严格地在正数和负数之间交替，则数字序列称为 摆动序列 。第一个差（如果存在的话）可能是正数或负数。仅有一个元素或者含两个不等元素的序列也视作摆动序列。

例如， [1, 7, 4, 9, 2, 5] 是一个 摆动序列 ，因为差值 (6, -3, 5, -7, 3) 是正负交替出现的。
相反，[1, 4, 7, 2, 5] 和 [1, 7, 4, 5, 5] 不是摆动序列，第一个序列是因为它的前两个差值都是正数，第二个序列是因为它的最后一个差值为零。
子序列 可以通过从原始序列中删除一些（也可以不删除）元素来获得，剩下的元素保持其原始顺序。

给你一个整数数组 nums ，返回 nums 中作为 摆动序列 的 最长子序列的长度 。

示例 1：
输入：nums = [1,7,4,9,2,5]
输出：6
解释：整个序列均为摆动序列，各元素之间的差值为 (6, -3, 5, -7, 3) 。

示例 2：
输入：nums = [1,17,5,10,13,15,10,5,16,8]
输出：7
解释：这个序列包含几个长度为 7 摆动序列。
其中一个是 [1, 17, 10, 13, 10, 16, 8] ，各元素之间的差值为 (16, -7, 3, -3, 6, -8) 。

示例 3：
输入：nums = [1,2,3,4,5,6,7,8,9]
输出：2
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
// 原始思路，存在某些情况下不符合
// var wiggleMaxLength = function(nums) {
//   const len = nums.length;
//   if (len === 1) return nums.length;
//   if (len === 2 && nums[0] !== nums[1]) return nums.length;
//   let res = [];
//   if (nums[0] !== nums[1]) {
//     res.push(nums[0], nums[1])
//   } else {
//     res.push(nums[1])
//   }
//   console.log('res-init', res)
//   for (let i = 2; i < len; i++) {
//     const temp = nums[0] !== nums[1] ? [nums[0], nums[1]] : [nums[1]]
//     let flag = 0;
//     if (nums[i] - nums[i - 1] === 0) {
//       continue;
//     } else {
//       flag = nums[i] - nums[i - 1] > 0;
//     }
//     for (let j = i; j < len; j++) {
//       flagTemp = nums[j] - nums[j - 1];
//       console.log(flagTemp, 'flagTemp--')
//       if (flagTemp !== 0 && !(flagTemp > 0) === flag) {
//         temp.push(nums[j]);
//         flag = !flag;
//       }
//     }
//     if (temp.length > res.length) {
//       res = temp;
//     }
//   }
//   return res;
// };

// 贪心算法
var wiggleMaxLength = function(nums) {
  if (nums.length <= 1) return 1;
  let res = 1,
  preDiff = 0,
  curDiff = 0;
  for (let i = 0; i < nums.length; i++) {
    curDiff = nums[i+1] - nums[i];
    if((curDiff > 0 && preDiff <= 0) || (curDiff < 0 && preDiff >= 0)) {
      res++;
      preDiff = curDiff;
    }
  }
  return res;
};
// 动态规划
var wiggleMaxLength = function(nums) {
  if (nums.length === 1) return 1;
    // 考虑前i个数，当第i个值作为峰谷时的情况（则第i-1是峰顶）
    let down = 1;
    // 考虑前i个数，当第i个值作为峰顶时的情况（则第i-1是峰谷）
    let up = 1;
    for (let i = 1; i < nums.length; i++) {
        if (nums[i] < nums[i - 1]) {
            down = Math.max(up + 1, down);
        }
        if (nums[i] > nums[i - 1]) {
            up = Math.max(down + 1, up)
        }
    }
    return Math.max(down, up);

};

// const demo1 = [1,7,4,9,2,5];
// const demo1 = [1,17,5,10,13,15,10,5,16,8];
// const demo1 = [1,2,3,4,5,6,7,8,9];
// const demo1 = [0,0,0];
const demo1 = [3,3,3,2,5];
console.time('start');
const res1 = wiggleMaxLength(demo1);
console.timeEnd('start');
console.log(res1);