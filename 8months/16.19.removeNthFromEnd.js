/**
 * 19. 删除链表的倒数第 N 个结点 【中等】
 * 给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function(head, n) {
  const virtualHeadNode = new ListNode(0, head);
  let slow = fast = virtualHeadNode;
  while (n-- > 0) fast = fast.next;
  while (fast.next != null) {
    slow = slow.next
    fast = fast.next
  }
  slow.next = slow.next.next;
  return virtualHeadNode.next;
};
const demo1 = [];
console.time('start');
const res1 = demoFunc(demo1);
console.timeEnd('start');
console.log(res1);