// 二分查找
var binarySearch = function (arr, target) {
  if (arr.length < 1) return - 1;
  let left = 0, right = arr.length - 1;
  while(left <= right) {
    let mid = Math.floor(left + (right - left)/2);
    console.log(mid, 'mid')
    if (arr[mid] === target) {
      return mid;
    } else if (arr[mid] > target) {
      right = mid - 1;
    } else {
      left = mid + 1;
    }
  }
  return -1;
}

const demo = [1,3,5,6,7,8,9,10,11];
const target = 5;
console.log(binarySearch(demo, target))

/*
int binary_search(int[] nums, int target) {
  int left = 0, right = nums.length - 1;
  while(left <= right) {
      int mid = left + (right - left) / 2;
      if (nums[mid] < target) {
          left = mid + 1;
      } else if (nums[mid] > target) {
          right = mid - 1;
      } else if(nums[mid] == target) {
          // 直接返回
          return mid;
      }
  }
  // 直接返回
  return -1;
}

int left_bound(int[] nums, int target) {
  int left = 0, right = nums.length - 1;
  while (left <= right) {
      int mid = left + (right - left) / 2;
      if (nums[mid] < target) {
          left = mid + 1;
      } else if (nums[mid] > target) {
          right = mid - 1;
      } else if (nums[mid] == target) {
          // 别返回，锁定左侧边界
          right = mid - 1;
      }
  }
  // 最后要检查 left 越界的情况
  if (left >= nums.length || nums[left] != target)
      return -1;
  return left;
}


int right_bound(int[] nums, int target) {
  int left = 0, right = nums.length - 1;
  while (left <= right) {
      int mid = left + (right - left) / 2;
      if (nums[mid] < target) {
          left = mid + 1;
      } else if (nums[mid] > target) {
          right = mid - 1;
      } else if (nums[mid] == target) {
          // 别返回，锁定右侧边界
          left = mid + 1;
      }
  }
  // 最后要检查 right 越界的情况
  if (right < 0 || nums[right] != target)
      return -1;
  return right;
}
*/