/**
 * 斐波那契数列，1,1,2,3,5,8,13。。。。
 * 
*/

/**
 * @param {number} n
 * @return {number}
 */
var fib = function(n) {
  if (n === 1 || n === 2) return 1
  return fib(n-1) + fib(n-2);
};

const demo = 40
console.time('start');
const res = fib(demo);
console.log(res)
console.timeEnd('start');

var memoryMap = new Map()
var fib1 = function(n) {
  if (n === 1 || n === 2) return 1;
  const m = fib1(n-1) + fib1(n-2);
  console.log(m, 'm')
  if (memoryMap.has(n)) {
    return memoryMap.get(n)
  } else {
    memoryMap.set(n, m)
  }
  return m;
};


// const demo1 = 10
// const res1 = fib1(demo1);
// memoryMap.forEach((val, key) => console.log(key, val))
// console.log(memoryMap.size)
// console.log(res1)

// 动态规划来解决
var fib3 = function(n) {
  const dp = [];
  dp[0] = 1, dp[1] = 1;
  for (let i = 2; i < n; ++i) {
    dp[i] = dp[i-1] + dp[i-2];
  }
  console.log(dp)
  return dp[n-1];
};

// const demo3 = 10
// const res3 = fib3(demo3);
// console.log(res3)

// 动态规划算法进一步优化，把空间复杂度降为 O(1)
var fib2 = function(n) {
  let pre = 1, cur = 1, next = 0;
  for (let i = 2; i < n; ++i) {
    next = pre + cur;
    pre = cur;
    cur = next;
    console.log(next, cur, pre)
  }
  return next;
};

// const demo2 = 10
// const res2 = fib2(demo2);
// console.log(res2)
