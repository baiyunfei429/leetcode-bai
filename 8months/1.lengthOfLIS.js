/**
 * 最长递增子序列
先安排一个非常火的题目，方便小伙伴们热热身

该算法在vue3 diff算法中有用到，作用是找到最长递归子序列后，可以减少子元素的移动次数

一个整数数组 nums，找到其中一组最长递增子序列的值
最长递增子序列是指：子序列中的所有元素单调递增
例如：[3,5,7,1,2,8] 的 LIS 是 [3,5,7,8]

亮点：网上一般都是只计算出最长递增子序列的长度，这里计算出一组具体的最长递增子序列的值
 *
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
// 自己盲写的，有问题，考虑的情况不全，有遗漏
var lengthOfLIS = function(nums) {
  if (!nums.length) return 0;
  const len = nums.length;
  const initNum = nums[0]
  console.log(initNum)
  let res = []//二位数组
  res.push([initNum])
  console.log(res)
  for (let i = 1; i < len; ++i) {
    const val = nums[i]
    const jLen = res.length;
    let flag = false // 表示此元素未进入任何数组
    for (let j = 0; j < jLen; ++j) {
      const childArrLen = res[j].length
      if (res[j][childArrLen - 1] < val) {
        res[j].push(val)
        flag = true // 表示进入某个数组里
      }
    }
    if (flag === false) {
      res.push([val])
    }
  }
  console.log(nums)
  return res;
};

// 看完一遍答案后，再默写的，采用动态规划方案
var lengthOfLIS2 = function(nums) {
  if (!nums.length) return 0;
  let dp = new Array(nums.length).fill(1); // 用于存放结果的
  console.log(dp, 'init')
  for (let i = 0; i < nums.length; ++i) {
    for (let j = 0; j < i; ++j) {
      if (nums[j] < nums[i]) {
        dp[i] = Math.max(dp[i], dp[j] + 1)
      }
    }
  }
  console.log(dp, 'dp')
  let res = 1;
  for (let i = 0; i < dp.length; ++i) {
    res = Math.max(res, dp[i])
  }
  return res;
};

// 测试
// console.log(lengthOfLIS2([10,9,2,5,3,7,101,18])); // 4
// console.log(lengthOfLIS2([0,1,0,3,2,3])); // 4
// console.log(lengthOfLIS2([7,7,7,7,7,7,7])); // 1

var lengthOfLIS3 = function(nums) {
  // 每堆的堆顶
  const top = [];
  // 牌堆数初始化为0
  let piles = 0;
  for (let i = 0; i < nums.length; i++) {
    // 要处理的扑克牌
    let poker = nums[i];
    // 左堆和最右堆进行二分搜索，因为堆顶是有序排的，最终找到该牌要插入的堆
    let left = 0,
      right = piles;
    //搜索区间是左闭右开
    while (left < right) {
      let mid = left + ((right - left) >> 1);
      if (top[mid] > poker) {
        right = mid;
      } else if (top[mid] < poker) {
        left = mid + 1;
      } else {
        right = mid;
      }
    }
    //  没找到合适的牌堆，新建一堆
    if (left == piles) piles++;
    // 把这张牌放到堆顶
    top[left] = poker;
    console.log(top)
  }
  console.log(top, ' --')
  return piles;
}
// 测试
// console.log(lengthOfLIS3([10,9,2,5,3,7,101,18])); // 4
// console.log(lengthOfLIS3([0,1,0,3,2,3])); // 4
// console.log(lengthOfLIS3([7,7,7,7,7,7,7])); // 1

var lengthOfLIS4 = function(nums) {
  const dp = new Array(nums.length).fill(1);
  for (let i = 0; i < dp.length; i++) {
    for (let j = 0; j < i; j ++) {
      if (nums[i] > nums[j]) {
        dp[i] = Math.max(dp[i], dp[j] + 1)
      }
    }
  }
  console.log(dp)
  let res = 1;
  for (let i = 0; i < dp.length; i++) {
    res = Math.max(res, dp[i])
  }
  return res;
};

console.log(lengthOfLIS4([10,9,2,5,3,7,101,18])); // 4