/**
 * 给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target  ，写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。
 *
示例 1:

输入: nums = [-1,0,3,5,9,12], target = 9
输出: 4
解释: 9 出现在 nums 中并且下标为 4
示例 2:

输入: nums = [-1,0,3,5,9,12], target = 2
输出: -1
解释: 2 不存在 nums 中因此返回 -1
 */
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
  let left = 0, right = nums.length - 1; // 定义target在左闭右闭的区间里，[left, right]
  while(left <= right) { // 当left==right，区间[left, right]依然有效，所以用 <=
    let mid = left + Math.floor((right - left)/2); // 防止溢出 等同于(left + right)/2
    if (target < nums[mid]) { // target 在左区间，所以[left, middle - 1]
      right = mid - 1
    } else if (target > nums[mid]) { // target 在右区间，所以[middle + 1, right]
      left = mid + 1
    } else { // nums[middle] == target
      return mid // 数组中找到目标值，直接返回下标
    }
  }
  return -1; // 未找到目标值
};
// 时间复杂度：O(log n)
// 空间复杂度：O(1)
// console.log(search([1,2,4,5,7,9], 6))

var search2 = function(nums, target) {
  let left = 0, right = nums.length; // 定义target在左闭右开的区间里，[left, right)
  while(left < right) { // 因为left == right的时候，在[left, right)是无效的空间，所以使用 <
    let mid = left + Math.floor((right - left)/2); // 防止溢出 等同于(left + right)/2
    if (target < nums[mid]) { // target 在左区间，所以[left, middle)
      right = mid
    } else if (target > nums[mid]) { // target 在右区间，所以[middle + 1, right)
      left = mid + 1
    } else { // nums[middle] == target
      return mid // 数组中找到目标值，直接返回下标
    }
  }
  return -1; // 未找到目标值
};
// 时间复杂度：O(log n)
// 空间复杂度：O(1)
console.log(search2([1,2,4,5,7,9], 4))