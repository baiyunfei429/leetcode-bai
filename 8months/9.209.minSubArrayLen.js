/**
 * 209. 长度最小的子数组 【中等】
 *给定一个含有 n 个正整数的数组和一个正整数 target 。

找出该数组中满足其和 ≥ target 的长度最小的 连续子数组 [numsl, numsl+1, ..., numsr-1, numsr] ，并返回其长度。如果不存在符合条件的子数组，返回 0 。

示例 1：
输入：target = 7, nums = [2,3,1,2,4,3]
输出：2
解释：子数组 [4,3] 是该条件下的长度最小的子数组。

示例 2：
输入：target = 4, nums = [1,4,4]
输出：1

示例 3：
输入：target = 11, nums = [1,1,1,1,1,1,1,1]
输出：0
*/

/**
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
var minSubArrayLen1 = function(target, nums) {
  let res = Number.MAX_VALUE; // 返回值
  let sum = 0; // 用来记录子序列的和
  let subLen = 0; // 子序列的长度
  const len = nums.length;
  for (let i = 0; i < len; i++) {
    sum = 0;
    for (let j = i; j < len; j++) {
      sum += nums[j];
      if (sum >= target) {
        subLen = j - i + 1;
        res = subLen > res ? res : subLen;
        break;
      }
    }
  }
  return res === Number.MAX_VALUE ? 0 : res;
};
// const demo1 = [2,3,1,2,4,3], target = 7;
// const demo1 = [1,4,4]; target = 4;
// const demo1 = [1,2,3,4,5]; target = 11;
// console.time('start');
// const res1 = minSubArrayLen1(target, demo1);
// console.timeEnd('start');
// console.log(res1);



var minSubArrayLen2 = function(target, nums) {
  let start = 0, end = 0;
  let sum = 0;
  let res = Infinity;
  let len = nums.length;
  while (end < len) {
    sum += nums[end];
    while (sum >= target) {
      res = Math.min(res, end - start + 1);
      sum -= nums[start];
      start++;
    }
    end++;
  }
  return res === Infinity ? 0 : res;
};

// const demo2 = [2,3,1,2,4,3], target = 7;
// const demo2 = [1,4,4]; target = 4;
// const demo2 = [1,2,3,4,5]; target = 11;
// console.time('start');
// const res2 = minSubArrayLen2(target, demo2);
// console.timeEnd('start');
// console.log(res2);



var minSubArrayLen3 = function(target, nums) {
  let start = 0, end = 0; // left 和 right
  let sum = 0;
  let res = nums.length + 1;
  let len = nums.length;
  while (end < len) {
    sum += nums[end];
    if (sum >= target) {
      // 不断移动左指针，直到不能再缩小为止
      while (sum - nums[start] >= target) {
        sum -= nums[start];
        start++;
      }
      res = Math.min(res, end - start + 1);
    }
    end++;
  }
  return res === nums.length + 1 ? 0 : res;
};

const demo3 = [2,3,1,2,4,3], target = 7;
// const demo3 = [1,4,4]; target = 4;
// const demo3 = [1,2,3,4,5]; target = 11;
console.time('start3');
const res3 = minSubArrayLen3(target, demo3);
console.timeEnd('start3');
console.log(res3);