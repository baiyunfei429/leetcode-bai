/**
 * 版本号排序

给定版本号['0.1.1', '2.3.3', '0.3002.1', '4.2', '4.3.5', '4.3.4.5']，输出版本号从小到大排序的结果。

思路：数组sort方法两参数代表任意两项，排序版本号问题就转变成了比较两版本大小的问题
 *
*/

var compareVersion = function(version1, version2) {
  const version1Arr = version1.split('.').map(e => e * 1);
  const version2Arr = version2.split('.').map(e => e * 1);
  const len = Math.max(version1Arr.length, version2Arr.length);
  for (let i = 0; i < len; i++) {
    if ((version1Arr[i] || 0) > (version2Arr[i] || 0)) return 1;
    if ((version1Arr[i] || 0) < (version2Arr[i] || 0)) return -1;
  }
  return 0;
};

var sortVersion = function(list) {
  return list.sort(compareVersion);
};
const versionList = ['0.1.1', '2.3.3', '0.3002.1', '4.2', '4.3.5', '4.3.4.5']
console.time('start');
const res1 = sortVersion(versionList);
console.timeEnd('start');
console.log(res1);