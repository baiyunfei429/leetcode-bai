// 柯里化，英语：Currying(果然是满满的英译中的既视感)，是把接受多个参数的函数变换成接受一个单一参数（最初函数的第一个参数）的函数，并且返回接受余下的参数而且返回结果的新函数的技术
// function add(a, b, c) {
//     return `a: ${a}, b: ${b}, c: ${c}`
// }

// function currying() {
// }

// const resFunc = currying(add)('bai')('yun')('fei')
// console.log(resFunc)

// 实现add(1)(2) =3
// const add = (num1) => (num2) => num1 + num2
// var res = add(1)(2)
// console.log(res);

// 可以无限链式调用 add(1)(2)(3)(4)(5)....
function add(x) {
    
}
