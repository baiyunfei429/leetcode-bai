/* 实现一个控制 promise 并发个数限制在特定个数的函数：
	1:例如：如需要总共发送 20个 fetch 请求，但是同时限制发送个数在 5 个以内包括5个，如果 5 个请求发送出去了还剩15个请求没有发送，
	 前5个请求任何一个请求成功（此题只考虑成功情况）就要将剩余15个中一个添加到4个正在请求的 pool里面，最终并发请求为5。
	2: 可以用 Promise.all, Promise.race, async, await 实现。
	3:
		* 入参：poolLimit（对当前请求池大小进行限制，如上例中就为5）,     
		 iteratorFns （返回 promise 实例的函数数组，如 [() => Promise.resolve(1) , () => Promise.resolve(2), () => Promise.resolve(3)]）  
		* 返回值：promise 执行结果（与第一个入参的 promsie 顺序保持一致）。
	*/
  const promiseList =	[() => Promise.resolve(1) , () => Promise.resolve(2), () => Promise.reject(3444), () => Promise.resolve(31), () => Promise.resolve(32)];
  const poolLimit = 3
  asyncPool(poolLimit, promiseList).then(res => console.log(res))
  
  function asyncPool(maxNum, promiseList) {
    return new Promise(resolve => {
      if (promiseList.length === 0) {
        return resolve([]);
        return;
      }
      const len = promiseList.length;
      const results = []; // 存放结果
      let index = 0; // 当前指针
      let resolveCount = 0; // 已经完成的数量
      async function next() {
        if (index === len) return;
        let i = index;
        index++;
        Promise.resolve(promiseList[i]).then(fn => fn()).then(res => {
          results[i] = res;
        }).catch(err => {
          results[i] = err;
        }).finally(() => {
          resolveCount++;
          if (resolveCount === len) {
            resolve(results);
          }
          next();
        })
      }
      const concurrencyNum = Math.min(maxNum, promiseList.length);
      for (let i = 0; i < concurrencyNum; i++) {
        next();
      }
    })
  }