// 用数组来模拟队列
// 1、单链队列；2、循环队列

// 单链队列
class Queue {
  constructor() {
  }
  enQueue(item) {
  }
  deQueue() {
  }
  getHeader() {
  }
  getLength() {
  }
  isEmpty() {
  }
}

// 循环队列
class SqQueue {
  constructor(length) {
  }
  enQueue(item) {
  }
  deQueue() {
  }
  getHeader() {
  }
  getLength() {
  }
  isEmpty() {
  }
  resize(length) {
  }
}