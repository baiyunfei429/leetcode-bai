// 首先需要定义节点： 
function Node(data, next) {
}

 // 链表
function NodeList(node){
}

// 遍历输出链表中各个结点的递归算法
function TraveList(head) {
}

// 创建链表（头插法）
function CreateListHead(num){
}

var newListByHead = CreateListHead(5)
console.log(newListByHead)
console.log(TraveList(newListByHead))

// 创建链表（尾插法）
function CreateListTail(num) {
}

var newListByTail = CreateListTail(7)
console.log(newListByTail)
console.log(TraveList(newListByTail))

// 将一个单向链表反转,使用三个变量分别表示当前节点和当前节点的前后节点，虽然这题很简单，但是却是一道面试常考题
function reverseList (head) {

}