/**
打平的数据内容
let arr = [
  {id: 1, name: '部门1', pid: 0},
  {id: 2, name: '部门2', pid: 1},
  {id: 3, name: '部门3', pid: 1},
  {id: 4, name: '部门4', pid: 3},
  {id: 5, name: '部门5', pid: 4},
]

输出结果
[
	{
		"id": 1,
		"name": "部门1",
		"pid": 0,
		"children": [
			{
				"id": 2,
				"name": "部门2",
				"pid": 1,
				"children": []
			},
			{
				"id": 3,
				"name": "部门3",
				"pid": 1,
				"children": [
					// 结果 ,,,
				]
			}
		]
	}
]
*/
const arr = [
  { id: 1, name: "部门1", pid: 0 },
  { id: 2, name: "部门2", pid: 1 },
  { id: 3, name: "部门3", pid: 1 },
  { id: 4, name: "部门4", pid: 3 },
  { id: 5, name: "部门5", pid: 4 },
];

const arrayToTree = (arr, pid = 0) => {
  return arr.reduce((prev, cur) => {
    if (cur.pid === pid) {
      const children = arrayToTree(arr, cur.id);
      if (children.length) {
        cur.children = children;
      }
      prev.push(cur);
    }
    return prev;
  }, []);
};
// const treeResult = arrayToTree(arr);
// console.dir(treeResult, { depth: null });
// console.log(JSON.stringify(treeResult, undefined, 2));

const treeToArray = (tree) => {
  return tree.reduce((prev, cur) => {
    if (!cur.children) {
      prev.push(cur);
    } else {
      const child = treeToArray(cur.children);
      delete cur.children;
      prev.push(cur, ...child);
    }
    return prev;
  }, []);
};
// const arrayResult = treeToArray(treeResult);
// console.log(arrayResult);

const arrayToTree2 = (arr, pid = 0) => {
  return arr
    .filter((item) => item.pid === pid)
    .map((item) => ({ ...item, children: arrayToTree2(arr, item.id) }));
};
// const treeResult2 = arrayToTree2(arr);
// console.dir(treeResult2, { depth: null });

const arrayToTree3 = (arr, rootPid = 0) => {
  const result = []; // 保存结果
  const map = new Map();

  for (let item of arr) {
    map.set(item.id, { ...item, children: [] });
  }

  for (const item of arr) {
    const { id, pid } = item;
    const treeItem = map.get(id);
    if (pid === rootPid) {
      result.push(treeItem);
    } else {
      // 下面的赋值没有必要
      // if (!map.get(pid)) {
      //   map.set(pid, { children: [] });
      // }
      map.get(pid).children.push(treeItem);
    }
  }
  return result;
};
// const treeResult3 = arrayToTree3(arr);
// console.dir("treeResult3");
// console.dir(treeResult3, { depth: null });

const arrayToTree4 = (items, rootPid = 0) => {
  const result = []; // 保存结果
  const itemMap = {};

  for (const item of items) {
    const { id, pid } = item;

    itemMap[id] = {
      ...item,
      children: [],
    };

    const treeItem = itemMap[id];

    if (pid === rootPid) {
      result.push(treeItem);
    } else {
      if (!itemMap[pid]) {
        itemMap[pid] = {
          children: [],
        };
      }
      itemMap[pid].children.push(treeItem);
    }
  }
  return result;
};
const treeResult4 = arrayToTree4(arr);
console.dir("treeResult4");
console.dir(treeResult4, { depth: null });
