/**
 * 题目名 手写 reduce
 * 
*/

// funA.reduce((total, curVal, index, array)=>total+curVal, init)
Array.prototype.myReduce = function() {
	const arr = this
	const fn = arguments[0]
	const init = arguments[1]
	let result = init
	for (let i = 0; i < this.length; i++) {
		result = fn(result, arr[i])
	}
	return result
};

const demo1 = [1,2,3,4,1]
const res1 = demo1.myReduce((a,b)=>a+b, 0);
console.log(res1)