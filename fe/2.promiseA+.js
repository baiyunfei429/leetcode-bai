// 实现一个符合A+规范的promise
// pending fulfilled rejected
function promiseAll(promiseArgs) {
  return new Promise((res, rej) => {
    const promiseArray = Array.from(promiseArgs);
    const len = promiseArray.length;
    const result = [];
    let count = 0;
    for (let i = 0; i < len; i++) {
      Promise.resolve(promiseArray[i]).then(r => {
        result[i] = r;
        if (++count === len) {
          res(result);
        }
      }).catch(e => rej(e))
    }
  })
}