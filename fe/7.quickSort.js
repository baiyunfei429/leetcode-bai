// 手写一个快排
function quickPartSort(arr, begin, end) {
	//定义左边下标和右边下标
	let left = begin;
	let right = end;
 
	//定义key值为左边第一个数据
  let pivotIndex = GetMidIndex(arr, left, right);
  if (left !== pivotIndex) { // 每次都将基准值放在left位置
    exchagne(arr, left, pivotIndex);
  }
	let key = arr[begin];
	//在左下标小于右下标时，继续比较
	while (left < right)
	{
		//再次比较两个下标防止越界风险，因为有一直找不到的风险
		//当右下标大于等于key值，会一直往后走，直到找到，才会停下来
		while (left < right && arr[right] >= key)
		{
			right--;
		}
		//左下标于key值比较，如果小于等于key，一直走，直到找到，停下来
		while (left < right && arr[left] <= key)
		{
			left++;
		}
		//当两个都停下来的时候，交换两个下标对应的值，让其继续走下去
		//有可能停下的条件是两个下标相等了，但是由于是同一个值，所以交换也没事
		exchagne(arr, left, right);
	}
	//最后相交点与key交换，区分两个大小区间，并使key到达正确位置
	exchagne(arr, begin, left);
  return left;
}
function quickSort (arr, begin, end) {
  if (begin >= end)
	{
		return;
	}
  if (end - begin <= 5) {
    InsertSort(arr, begin, end);
    return;
  } else {
    const partition = quickPartSort(arr, begin, end);
    //让后将上述操作细分为子问题，把左右两个无序区间再次按照该操作执行
    quickSort(arr, begin, partition - 1);
    quickSort(arr, partition + 1, end);
  }
}

function InsertSort(arr, left, right) {
  // 插入排序
  for (let i = left + 1; i < right + 1; i++) {
    let j = i;
    let target = arr[j]; // 要插入的元素
    while (j > left && arr[j - 1] > target) {
      arr[j] = arr[j - 1];
      j--;
    }
    arr[j] = target;
  }
  // return arr; //此处不需要return，因为会修改原数组
}
// 交换
function exchagne(arr, a, b) {
  var temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
}
function GetMidIndex (arr, begin, end) {
  let mid = begin + Math.floor((end - begin)/2);
  if (arr[begin] > arr[end]) {
    if (arr[end] > arr[mid]) {
      return end;
    } else if (arr[begin] > arr[mid]) {
      return mid;
    } else {
      return begin;
    }
  } else {
    if (arr[begin] > arr[mid])
			return end;
		else if (arr[begin] > arr[mid])
			return mid;
		else
			return begin;
  }
}

// const arr = [2,3,5,1,8,6,4,7];
// const len = arr.length;
// quickSort(arr, 0, len - 1);
// console.log(arr, '----arr');

// 三路快排
/*
* @param arr 需要进行三路快排的数组
* @param L 数组的起始位置
* @param R 数组的末尾位置
* @returns {{lt: *, gt: *}}
如图所示，我们将数组中的0号元素设为基准值(piovt)，设为p，将元素分为小于p,等于p，大于p三个部分。
元素i指向当前进行比较的元素，L为数组的起点，R为数组的末尾。
区间[L+1,lt]是小于p的元素，区间[lt+1,i-1]是等于p的元素，从右侧的R往内，形成的区间[gt,R]存放的是大于P的元素。
https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/4/14/17174cdd6cce6df4~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.awebp

排序一开始，这些区间都是不存在的，我们需要确定边界，i的开始索引指向L+1，lt的初始值L，而gt的初始值是则是R+1，表示这三个区间均为空；
用JS实现三路快排
我们将上述图解整理下，得出的实现思路如下：

如果当前i指向的元素等于p，则i+1
如果当前i指向的元素小于p，则将lt+1处的元素与索引i处的值进行交换，然后lt+1,并且i+1
如果当前i指向的元素大于p，则将gt-1处的元素与索引i处的值进行交换，然后gt-1
最后当i走到gt处时，即gt==i时；那就说明，除了第一个元素之外，其余的空间已经分区完毕，只要将首个元素与lt处的元素进行交换，然后lt-1；我们就形成了想要的三个区间，小于p，等于p，大于p

原文链接：https://juejin.cn/post/6844904126266998797
*/
const partition = function (arr, L, R) {
   // 基准值为数组的零号元素
   let p = arr[L];
   // 左区间的初始值: L
   let lt = L;
   // 右区间的初始值: R+1
   let gt = R + 1;
   for (let i = L + 1; i < gt;){
       if(arr[i] === p){
           // 当前i指向的元素等于p
           i++;
       } else if(arr[i] > p){
           // 当前i指向的元素大于p，将gt-1处的元素与当前索引处的元素交换位置，gt--
           [arr[gt -1],arr[i]] = [arr[i],arr[gt - 1]];
           gt--;
       }else{
           // 当前i指向的元素小于p，将lt+1处的元素与当前索引处的元素交换位置，lt+1，i+1
           [arr[lt + 1],arr[i]] = [arr[i],arr[lt + 1]];
           lt++;
           i++;
       }
   }

   // i走向gt处，除了基准值外的元素，其余的空间已经分区完毕，交换基准值与lt处的元素，lt-1，最终得到我们需要的三个区间
   [arr[L],arr[lt]] = [arr[lt],arr[L]];
   lt--;
   console.log(`三路快排后的数组: ${arr}`);
   return {lt : lt, gt : gt};
}

const threeWayFastRow = function (arr,L,R) {
  // 当前数组的起始位置大于等于数组的末尾位置时退出递归
  if(L >= R){
      return false;
  }
  let obj = partition(arr, L, R);
  // 递归执行: 将没有大于p,和小于p区间的元素在进行三路快排
  threeWayFastRow(arr,L,obj.lt);
  threeWayFastRow(arr,obj.gt,R);
}

console.time("三路快排");
const dataArr = [3,5,8,1,2,9,4,7,6];
threeWayFastRow(dataArr,0,dataArr.length - 1);
console.log(`三路快排完成: ${dataArr}`);
console.timeEnd("三路快排");
