// 手写实现sleep
// (async () => {
// 	console.log('start');
// 	await sleep(3000)
// 	console.log('end');

// 	function sleep(delay) {
// 		return new Promise((res, rej) => {
// 			setTimeout(() => {
// 				res()
// 			}, delay)
// 		})
// 	}
// })()

(async () => {
	console.log('start');
	await sleep(3000)
	console.log('end');
	function sleep(delay) {
		const startTime = new Date().getTime()
		while(new Date().getTime() - startTime < delay) {
			console.log(new Date().getTime());
			continue;
		}
	}
})()

// 实现一个sleep方法
const sleep = delay => {
  for (let start = Date.now(); Date.now() - start <= delay;) {}
}
() => {
	console.log('task1 start')
	sleep(20) // 实现了等待20毫秒
	console.log('task1 end')
}
