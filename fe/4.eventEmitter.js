// 手写一个发布-订阅者模式
// 使用 JS 实现一个发布订阅器，Event，示例如下:
/**
const e = new Event()
e.on('click', x => console.log(x.id))
e.once('click', x => console.log(id))
//=> 3
e.emit('click', { id: 3 })
//=> 4
e.emit('click', { id: 4 })
API 如下：
class Event {
  emit (type, ...args) {
  }

  on (type, listener) {
  }

  once (type, listener) {
  }

  off (type, listener) {
  }
}
 */
class Event1 {
  constructor() {
    this.event = {};
  }
  emit (type, ...args) {
    if(this.event[type]) {
      this.event[type].forEach(fn => {
        fn.apply(this, args);
      })
    }
  }

  on (type, listener) {
    if (!this.event[type]) this.event[type] = [];
    this.event[type].push(listener)
  }

  once (type, listener) {
    function f(...args) {
      listener(...args);
      this.off(type, f)
    }
    this.on(type, f);
  }

  off (type, listener) {
    if(this.event[type]) {
      this.event[type] = this.event[type].filter(item => item !== listener);
    }
  }
}
class Event {
  events = {}

  emit (type, ...args) {
    const listeners = this.events[type]
    for (const listener of listeners) {
      listener(...args)
    }
  }

  on (type, listener) {
    this.events[type] = this.events[type] || []
    this.events[type].push(listener)
  }

  once (type, listener) {
    const callback = (...args) => {
      this.off(type, callback)
      listener(...args)
    }
    this.on(type, callback)
  }

  off (type, listener) {
    this.events[type] = this.events[type] || []
    this.events[type] = this.events[type].filter(callback => callback !== listener)
  }
}

const e = new Event()

const callback = x => { console.log('Click', x.id) }
e.on('click', callback)
e.on('click', callback)

// 只打印一次
const onceCallback = x => console.log('Once Click', x.id)
e.once('click', onceCallback)
e.once('click', onceCallback)

//=> 3
e.emit('click', { id: 3 })

//=> 4
e.emit('click', { id: 4 })