/**
 * 118. 杨辉三角【简单】
 * 给定一个非负整数 numRows，生成「杨辉三角」的前 numRows 行。
在「杨辉三角」中，每个数是它左上方和右上方的数的和。

示例 1:
输入: numRows = 5
输出: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]

示例 2:
输入: numRows = 1
输出: [[1]]
*/

/**
 * @param {number} numRows
 * @return {number[][]}
 */
var generate = function(numRows) {
  if (numRows === 1) return [[1]]
  if (numRows === 2) return [[1], [1,1]]
  let res = [[1], [1,1]]
  for (let i = 3; i <= numRows; i ++) {
    let nVal = [1] // 0位置的1作为初始值
    const lastRes = res[i-2]
    // 1. 首先把res[n]的数据搞出来
    for (j = 1; j < i - 1; j++) {
      nVal[j] = lastRes[j-1] + lastRes[j]
    }
    nVal.push(1) // 最后一个1，push进来
    // 2. 再push到res里
    res.push(nVal)
  }
  return res
};
var generate2 = function(numRows) {
  let res = []
  if (numRows >= 1) res.push([1])
  if (numRows >= 2) res.push([1,1])
  for (let i = 3; i <= numRows; i ++) {
    let nVal = [1] // 0位置的1作为初始值
    // 1. 首先把res[n]的数据搞出来
    for (j = 1; j < i - 1; j++) {
      nVal[j] = res[i-2][j-1] + res[i-2][j]
    }
    nVal.push(1) // 最后一个1，push进来
    // 2. 再push到res里
    res.push(nVal)
  }
  return res
};
var generate3 = function(numRows) {
  const res = []
  for (let i = 0; i < numRows; i ++) {
    const row = new Array(i + 1).fill(1)
    for (j = 1; j < row.length - 1; j++) {
      row[j] = res[i-1][j-1] + res[i-1][j]
    }
    res.push(row)
  }
  return res
};

const demo1 = 5
const res1 = generate3(demo1);
console.log(res1)