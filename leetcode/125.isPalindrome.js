/**
 * 125. 验证回文串 【简单】
 * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
说明：本题中，我们将空字符串定义为有效的回文串。

示例 1:
输入: "A man, a plan, a canal: Panama"
输出: true
解释："amanaplanacanalpanama" 是回文串

示例 2:
输入: "race a car"
输出: false
解释："raceacar" 不是回文串

*/

/**
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function(s) {
  if (s === '') return true
  s = s.replace(/[^a-zA-Z0-9]/g, '').toLowerCase()
  const sLen = s.length
  let flag = true
  for (let i = 0; i <= Math.floor(sLen / 2); i ++) {
    if (s[i] !== s[sLen - 1 - i]) {
      flag = false
      break
    }
  }
  return flag
};
const demo1 = "A man, a plan, a canal: Panama"
const res1 = isPalindrome(demo1);
console.log(res1)