/**
 * 21. 合并两个有序链表
 * 
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 

示例 1：
输入：l1 = [1,2,4], l2 = [1,3,4]
输出：[1,1,2,3,4,4]

示例 2：
输入：l1 = [], l2 = []
输出：[]

示例 3：
输入：l1 = [], l2 = [0]
输出：[0]

提示：
两个链表的节点数目范围是 [0, 50]
-100 <= Node.val <= 100
l1 和 l2 均按 非递减顺序 排列
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
// l1: 1 4 6
// l2: 5 6 7
// 再次遇见还是不熟，特别是对于l1.next = mergeTwoLists(l1.next, l2)，直接使用递归了就
// 
var mergeTwoLists = function(l1, l2) {
  if (l1 === null) return l2
  if (l2 === null) return l1
  if (l1.val <= l2.val) {
    // 判断l1节点值<l2节点值，那么，把l1的值取出来，作为起点，再把l1.next和l2进行比较和合并，并将结果放在取出来的l1的值的后面
    // 即l1.next = 继续排序(l1.next, l2) => l1.next = mergeTwoLists(l1.next, l2)，并把l1进行return
    l1.next = mergeTwoLists(l1.next, l2)
    return l1
  } else {
    l2.next = mergeTwoLists(l1, l2.next)
    return l2
  }
}

const demo1 = '()[]{}'
const res1 = mergeTwoLists(demo1)
console.log(res1)

