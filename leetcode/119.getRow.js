/**
 * 119. 杨辉三角 II【简单】
 * 给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。
在「杨辉三角」中，每个数是它左上方和右上方的数的和。

示例 1:
输入: rowIndex = 3
输出: [1,3,3,1]

示例 2:
输入: rowIndex = 0
输出: [1]

示例 3:
输入: rowIndex = 1
输出: [1,1]
*/

/**
 * @param {number} rowIndex
 * @return {number[]}
 */
var getRow = function(rowIndex) {
  let lastVal = [1]
  for (let i = 0; i <= rowIndex; i++) {
    const row = new Array(i + 1).fill(1)
    for (let j = 1; j < i; j ++) {
      row[j] = lastVal[j-1] + lastVal[j]
    }
    lastVal = row
  }
  return lastVal
};
var getRow2 = function(rowIndex) {
  const res = new Array(rowIndex + 1).fill(0)
  res[0] = 1
  for (let i = 1; i <= rowIndex; ++i) {
    for (let j = i; j > 0; --j) {
      res[j] += res[j - 1];
    }
  }
  return res
};

const demo1 = 5
const res1 = getRow(demo1);
console.log(res1)