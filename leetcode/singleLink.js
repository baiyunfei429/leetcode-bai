// 首先需要定义节点： 
function Node(data, next) {
  this.data = data;
  this.next = next;
}

 // 链表
function NodeList(node){
  this.length = 0;
  this.next = node;
}

// 遍历输出链表中各个结点的递归算法
function TraveList(head) {
	if (head == null)
		return;     //递归终止
	else {
    console.log(head.data) // 输出当前结点的数据域
		TraveList(head.next);    // head指向后继指点继续递归
	}
}

// 创建链表（头插法）
function CreateListHead(num){
  var list = new NodeList(null)
  for (let i = 0; i < num; i += 1) {
    // 1. 先创建一个节点，使用随机数作为data
    const node = new Node(Math.random() * 100, null)
    // 2. 把新创建的节点的指针next指向单链表的指针next
    node.next = list.next
    // 3. 把单链表的指针next指向新创建的节点
    // 其中，2和3不能颠倒，为了保证指针指向的正确性，必须先把新节点的指针矫正，然后才能修改原来头结点的指针
    list.next = node
    // 4. length自增
    list.length++
  }
  return list
}

var newListByHead = CreateListHead(5)
console.log(newListByHead)
console.log(TraveList(newListByHead))

// 创建链表（尾插法）
function CreateListTail(num) {
  var list = new NodeList(null)
  var p = list // 获取当前链表尾节点的信息
  for (let i = 0; i < num; i += 1) {
    // 1. 先创建一个节点，使用随机数作为data
    let node = new Node(Math.random() * 100, null)
    // 2. 先把每次取出来的当前链表的尾节点指向新创建的节点，改动作即可完成新节点的尾插入
    p.next = node
    // 3. 但是为了下一次插入，需要把当前链表的尾节点取出来，赋值给p
    p = node
    // 4. length自增
    list.length++
  }
  return list
}

var newListByTail = CreateListTail(7)
console.log(newListByTail)
console.log(TraveList(newListByTail))