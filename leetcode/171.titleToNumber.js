/**
 * 171. Excel 表列序号 【简单】
 * 给你一个字符串 columnTitle ，表示 Excel 表格中的列名称。返回该列名称对应的列序号。

例如，
    A -> 1
    B -> 2
    C -> 3
    ...
    Z -> 26
    AA -> 27
    AB -> 28 
    ...

示例 1:
输入: columnTitle = "A"
输出: 1

示例 2:
输入: columnTitle = "AB"
输出: 28

示例 3:
输入: columnTitle = "ZY"
输出: 701

示例 4:
输入: columnTitle = "FXSHRXW"
输出: 2147483647
*/

/**
 * @param {string} columnTitle
 * @return {number}
 */
var titleToNumber = function(columnTitle) {
  let res = 0
  while(columnTitle.length > 0) {
    res = res * 26 + (columnTitle[0].charCodeAt() - 64)
    columnTitle = columnTitle.substring(1)
  }
  return res
};
var titleToNumber2 = function(columnTitle) {
  let res = 0
  let len = columnTitle.length
  let index = 0
  while(index < len) {
    res = res * 26 + (columnTitle[index].charCodeAt() - 64)
    index++
  }
  return res
};

const demo1 = "FXSHRXW"
const res1 = titleToNumber2(demo1);
console.log(res1)