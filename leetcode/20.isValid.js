/**
 * 20. 有效的括号
 * 
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
有效字符串需满足：
左括号必须用相同类型的右括号闭合。
左括号必须以正确的顺序闭合。
 
示例 1：
输入：s = "()"
输出：true

示例 2：
输入：s = "()[]{}"
输出：true

示例 3：
输入：s = "(]"
输出：false

示例 4：
输入：s = "([)]"
输出：false

示例 5：
输入：s = "{[]}"
输出：true
 
提示：
1 <= s.length <= 104
s 仅由括号 '()[]{}' 组成

*/
/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
  // 我需要一个栈，每次入栈前判断是否与栈顶元素匹配，并且map[元素]>0，满足则出栈，不满足则入栈
  const stack = []
  const sLen = s.length
  const map = {
    '(': -1,
    ')': 1,
    '[': -2,
    ']': 2,
    '{': -3,
    '}': 3,
  }
  for(let i = 0; i < sLen; i += 1) {
    const stackLen = stack.length
    if (i > 0 && map[s[i]] > 0 && map[stack[stackLen - 1]] + map[s[i]] === 0) {
      stack.pop()
    } else {
      stack.push(s[i])
    }
  }
  return stack.length === 0
}

const demo1 = '()[]{}'
const res1 = isValid(demo1)
console.log(res1)

