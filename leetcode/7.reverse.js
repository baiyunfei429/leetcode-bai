/**
 * 
整数反转

给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
假设环境不允许存储 64 位整数（有符号或无符号）。

示例 1：

输入：x = 123
输出：321
示例 2：

输入：x = -123
输出：-321
示例 3：

输入：x = 120
输出：21
示例 4：

输入：x = 0
输出：0

 */

var reverse = function(x) {
  if (x === 0) {
    return 0
  }
  var isLtZero = false
  if (x < 0) {
      isLtZero = true
      x = Math.abs(x)
  }
  var result = 0
  while (x > 0) {
      const num = x % 10
      x = Math.floor(x / 10)
      result = result * 10 + num
  }
  if (result < (-Math.pow(2, 31)) || result > (Math.pow(2, 31) - 1)) {
      return 0
  }
  return isLtZero ? -result : result
}

var reverse2 = function(x) {
  var oldNum = Math.abs(x);
  var nowNum = 0;
  // 下面这种模式要记得，经常用到，遍历一个大数的全部数字，用作反转之类的操作
  while(oldNum > 0) {
    nowNum = nowNum * 10 + oldNum % 10;
    oldNum = Math.floor(oldNum / 10);
  }

  if(x < 0) {
    return nowNum <= Math.pow(2, 31) ? -nowNum : 0;
  } else {
    return nowNum < Math.pow(2, 31) ? nowNum : 0;
  }
}

const x_demo_1 = 123
var result_demo_1 = reverse(x_demo_1)
console.log(result_demo_1)