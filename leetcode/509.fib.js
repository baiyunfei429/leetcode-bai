/**
 * 509. 斐波那契数 【简单】
 * 斐波那契数，通常用 F(n) 表示，形成的序列称为 斐波那契数列 。该数列由 0 和 1 开始，后面的每一项数字都是前面两项数字的和。也就是：

F(0) = 0，F(1) = 1
F(n) = F(n - 1) + F(n - 2)，其中 n > 1
给你 n ，请计算 F(n) 。

示例 1：
输入：2
输出：1
解释：F(2) = F(1) + F(0) = 1 + 0 = 1

示例 2：
输入：3
输出：2
解释：F(3) = F(2) + F(1) = 1 + 1 = 2

示例 3：
输入：4
输出：3
解释：F(4) = F(3) + F(2) = 2 + 1 = 3
 
提示：
0 <= n <= 30
*/

/**
 * @param {number} n
 * @return {number}
 */
// 方法1：普通递归
var fib1 = function(n) {
  if (n <= 1) return n
  return fib1(n-1) + fib1(n-2)
};
// 方法2：改进递归-利用闭包特性把运算结果存储在数组里，避免重复计算
var fib2 = function(n) {
  let memo = [0, 1]
  let f = function(n) {
    if (memo[n] === undefined) {
      return f(n-1) + f(n-2)
    }
    return memo[n]
  }
  return f
}();
// 方法3
var fib = function() {
  let memo = new Map()
  let f = function(n) {
    if (n ===0) return 0
    if (n === 1 || n === 2) {
      return 1
    }
    let memorised = memo.get(n)
    if (memorised !== undefined) {
      return memorised
    }
    let f1 = fib(n-1)
    let f2 = fib(n-2)
    memo.set(n-1, f1)
    memo.set(n-2, f2)
    return f1 + f2
  }
  return f
}();
// 方法4: 继续改进递归-抽离存储功能的方法
var memorise = function(func) {
  let memo = []
  return function (n) {
    if (memo[n] === undefined) {
      memo[n] = func(n)
    }
    return memo[n]
  }
};
var fib3 = memorise(function(n) {
  if (n === 1 || n === 2) {
    return 1
  }
  return fib3(n - 2) + fib3(n - 1)
})
/**
 * 方法1-方法4，都是采用自顶向下的解法。在递归的过程中，这些调用栈是不断增加的，当栈的层级足够多时，就会爆栈：Maximum call stack size exceeded
 * 所以需要换个思路，自底向上。从最小值开始求解，然后每次都保存结果，方便较大的值使用较小值直接相加，完成计算。摒弃递归的逻辑
 */

// 方法5: 使用循环解决
function fib4(n) {
  if (n < 1) return 0
  if (n <= 2) return 1
  var n1 = 1, n2 = 1, sum
  for (let i = 2; i < n; i++) {
    sum = n1 + n2
    n1 = n2
    n2 = sum
  }
  return sum
}
// 方法6: 循环+解构
var fib5 = function (n) {
  if (n < 1) return 0
  if (n <= 2) return 1
  let n1 = 1, n2 = 1
  for (let i = 2; i < n; i++) {
    [n1, n2] = [n2, n1 + n2]
  }
  return n2
}
// 方法7：
var fib7 = function (n) {
  let res = []
  res[0] = 0
  res[1] = 1
  for (let i = 2; i < n; i++) {
    res[i] = res[i-1] + res[i-2]
  }
  return res[n]
}
// 方法8：大数， BigInt 的数据类型
var fib7 = function (n) {
  let res = []
  res[0] = 0n // 此处有区别
  res[1] = 1n // 此处有区别
  for (let i = 2; i < n; i++) {
    res[i] = res[i-1] + res[i-2]
  }
  return res[n]
}

const demo1 = 30
const res1 = fib(demo1);
console.log(res1)