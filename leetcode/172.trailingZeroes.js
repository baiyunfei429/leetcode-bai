/**
 * 172. 阶乘后的零 【简单】
 * 给定一个整数 n，返回 n! 结果尾数中零的数量。
示例 1:
输入: 3
输出: 0
解释: 3! = 6, 尾数中没有零。

示例 2:
输入: 5
输出: 1
解释: 5! = 120, 尾数中有 1 个零.

说明: 你算法的时间复杂度应为 O(log n) 。
*/

/**
 * @param {number} n
 * @return {number}
 */
// 没想明白，为啥这个方法不行，某些用例过不去。
// 明白了，因为下面这个优化后的方法是每次乘以数字后，就进行取余判断和除以10的操作，并且内层while是plusRes>0作为判断的，因此会把不是末尾连续的0计算在内，即中间存在的0
// 所以，下面的方法是错误的
var trailingZeroes_Error = function(n) {
  let plusRes = 1
  let res = 0
  let flag = true
  while(n > 1) {
    plusRes = plusRes * n
    while(plusRes > 0 && plusRes % 10 === 0) {
      res++
      plusRes = Math.floor(plusRes/10)
    }
    --n
  }
  return res
};
// 下面这种方法30的用例过不去，返回0，猜测跟number的最大数有关，即超出了
var trailingZeroes_Error2 = function(n) {
  let plusRes = 1
  let res = 0
  let flag = true
  while(n > 1) {
    plusRes = plusRes * n
    --n
  }
  console.log(plusRes,"plis")
  while(plusRes > 0 && plusRes % 10 === 0) {
    res++
    plusRes = Math.floor(plusRes/10)
  }
  return res
}
var trailingZeroes = function(n) {
  let plusRes = 1
  let res = 0
  let flag = true
  while(n > 1) {
    plusRes = plusRes * n
    while(plusRes > 0 && plusRes % 10 === 0) {
      res++
      plusRes = Math.floor(plusRes/10)
    }
    --n
  }
  return res
}
var trailingZeroes2 = function(n) {
  let r = 0;
  while (n > 1) {
    n = parseInt(n / 5);
    r += n;
  }
  return r;
};

const demo1 = 30
const res1 = trailingZeroes(demo1);
console.log(res1)