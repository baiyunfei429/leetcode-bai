// 柯里化，英语：Currying(果然是满满的英译中的既视感)，是把接受多个参数的函数变换成接受一个单一参数（最初函数的第一个参数）的函数，并且返回接受余下的参数而且返回结果的新函数的技术
function add(a, b, c) {
    return `a: ${a}, b: ${b}, c: ${c}`
}

function currying(fn, arr = []) {
    let len = fn.length
    return (...args) => {
        arr = [...arr, ...args]
        if (arr.length < len) {
            return currying(fn, arr)
        } else {
            return fn(...arr)
        }
    }
}

const resFunc = currying(add)('bai')('yun')('fei')
console.log(resFunc)