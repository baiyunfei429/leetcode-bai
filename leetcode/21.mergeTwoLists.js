/**
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 

输入：l1 = [1,2,4], l2 = [1,3,4]
输出：[1,1,2,3,4,4]
示例 2：

输入：l1 = [], l2 = []
输出：[]
示例 3：

输入：l1 = [], l2 = [0]
输出：[0]
 */

// 补充下：这个只是合并两个有序数组，而不是有序链表，因为链表的数据结构不是这样的
var mergeTwoArrays = function(l1, l2) {
  const len1 = l1.length
  let len2 = l2.length
  if (len1 < 1) return l2
  if (len2 < 1) return l1
  for (let i = 0; i < len1; i += 1) {
    let flag = true
    for(let j = 0; j < len2 - 1; j += 1) {
      if (l1[i] <= l2[j]) {
        l2.splice(j, 0, l1[i])
        flag = false
        break;
      } else if (l1[i] > l2[j] && l1[i] < l2[j+1]) {
        l2.splice(j + 1, 0, l1[i])
        flag = false
        break;
      }
    }
    if (flag) {
      l2.push(l1[i])
    }

  }
  return l2
}

const demo_l1 = [1,2,4], demo_l2 = [1,3,4]
const demo_result = mergeTwoArrays(demo_l1, demo_l2)
console.log('demo_l1: ', [1,2,4])
console.log('demo_l2: ', [1,3,4])
console.log('demo_result: ', demo_result)

/**
 * Definition for singly-linked list
 * function ListNode(val) {
    this.val = val;
    this.next = null;
  }
 */ 
var mergeTwoLists = function(l1, l2) {
  if (l1 === null) returl l2
  if (l2 === null) returl l1
  if (l1.val <= l2.val) {
    l1.next = mergeTwoLists(l1.next, l2)
    return l1
  } else {
    l2.next = mergeTwoLists(l2.next, l1)
    return l2
  }
};