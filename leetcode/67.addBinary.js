/**
 * 67. 二进制求和 【简单】
 * 给你两个二进制字符串，返回它们的和（用二进制表示）。
输入为 非空 字符串且只包含数字 1 和 0。

示例 1:
输入: a = "11", b = "1"
输出: "100"

示例 2:
输入: a = "1010", b = "1011"
输出: "10101"

提示：
每个字符串仅由字符 '0' 或 '1' 组成。
1 <= a.length, b.length <= 10^4
字符串如果不是 "0" ，就都不含前导零。
*/

/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 */
// 1.从尾部往头部开始，如果a === b && a === '1',那么进位是1，本地num = plus
// else, 进位是0， 本地num = plus
// 2.a!==b,那么,进位是plus === '1' ? '1' : '0',本地num = plus === '1' ? '0' : '1'
// 临界值：
var addBinary = function(a, b) {
  if (a === '0') return b
  if (b === '0') return a
  const lenA = a.length, lenB = b.length
  const len = Math.max(lenA, lenB)
  let res = [], plus = '0'
  for (let i = 0; i < len; i++) {
    const aNum = a[lenA - 1 - i] || '0'
    const bNum = b[lenB - 1 - i] || '0'
    if (aNum === bNum) {
      num = plus
      plus = aNum === '1' ? '1' : '0'
    } else { // aNum !== bNum
      num = plus === '1' ? '0' : '1'
      plus = plus === '1' ? '1' : '0'
    }
    res.unshift(num)
  }
  if(plus === '1') {
    res.unshift('1')
  }
  return res.join('')
};
// 能用字符串，不用数组
var addBinary2 = function(a, b) {
  if (a === '0') return b
  if (b === '0') return a
  const lenA = a.length, lenB = b.length
  const len = Math.max(lenA, lenB)
  let res = '', plus = '0'
  for (let i = 0; i < len; i++) {
    const aNum = a[lenA - 1 - i] || '0'
    const bNum = b[lenB - 1 - i] || '0'
    if (aNum === bNum) {
      num = plus
      plus = aNum === '1' ? '1' : '0'
    } else { // aNum !== bNum
      num = plus === '1' ? '0' : '1'
      plus = plus === '1' ? '1' : '0'
    }
    res = num + res
  }
  if(plus === '1') {
    res = '1' + res
  }
  return res
};

const a = "11", b = "1"
const res1 = addBinary(a, b);
console.log(res1)