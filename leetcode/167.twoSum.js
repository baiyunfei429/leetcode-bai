/**
 * 167. 两数之和 II - 输入有序数组 【简单】
 * 给定一个已按照 升序排列  的整数数组 numbers ，请你从数组中找出两个数满足相加之和等于目标数 target 。
函数应该以长度为 2 的整数数组的形式返回这两个数的下标值。numbers 的下标 从 1 开始计数 ，所以答案数组应当满足 1 <= answer[0] < answer[1] <= numbers.length 。
你可以假设每个输入只对应唯一的答案，而且你不可以重复使用相同的元素。
 
示例 1：
输入：numbers = [2,7,11,15], target = 9
输出：[1,2]
解释：2 与 7 之和等于目标数 9 。因此 index1 = 1, index2 = 2 。

示例 2：
输入：numbers = [2,3,4], target = 6
输出：[1,3]

示例 3：
输入：numbers = [-1,0], target = -1
输出：[1,2]
 
提示：
2 <= numbers.length <= 3 * 104
-1000 <= numbers[i] <= 1000
numbers 按 递增顺序 排列
-1000 <= target <= 1000
仅存在一个有效答案
*/

/**
 * @param {number[]} numbers
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(numbers, target) {
  const len = numbers.length
  for (let i = 0; i < len; i++) {
    const dest = target - numbers[i]
    for (let j = i + 1; j < len; j++) {
      if (dest === numbers[j]) {
        return [i + 1, j + 1]
      }
    }
  }
};
var twoSum2 = function(numbers, target) {
  const len = numbers.length
  const map = new Map()
  for (let i = 0; i < len; i++) {
    const dest = target - numbers[i]
    if (map.get(dest) === undefined) {
      for (let j = i + 1; j < len; j++) {
        if (dest === numbers[j]) {
          return [i + 1, j + 1]
        } else {
          map.set(numbers[j], j)
        }
      }
    } else {
      return [i + 1, map.get(dest) + 1]
    }
  }
};
//输入：numbers = [2,3,4], target = 6
// 输出：[1,3]
var twoSum3 = function(numbers, target) {
  const len = numbers.length
  const map = new Map()
  for (let i = 0; i < len; i++) {
    const dest = target - numbers[i]
    if (map.get(dest) !== undefined) {
      return [map.get(dest)+1, i + 1]
    } else {
      map.set(numbers[i], i)
    }
  }
};
// 双指针,时间复杂度小了，快了，空间复杂度增加了
var twoSum4 = function(numbers, target) {
  let [left, right] = [0, numbers.length - 1]
  while (left < right) {
    if (numbers[left] + numbers[right] < target) {
      left++
    } else if (numbers[left] + numbers[right] > target) {
      right--
    } else {
      return [left+1, right+1]
    }
  }
};

const demo1 = [2,3,4], target = 6
const demo2 = [-1,0], target2 = -1
const demo3 = [5,25,75], target3 = 100
const res1 = twoSum4(demo1, target);
console.log(res1)
const res2 = twoSum4(demo2, target2);
console.log(res2)
const res3 = twoSum4(demo3, target3);
console.log(res3)