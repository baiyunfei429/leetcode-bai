/**
 * 169. 多数元素 【简单】
 * 给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
你可以假设数组是非空的，并且给定的数组总是存在多数元素。

示例 1：
输入：[3,2,3]
输出：3

示例 2：
输入：[2,2,1,1,1,2,2]
输出：2
 
进阶：
尝试设计时间复杂度为 O(n)、空间复杂度为 O(1) 的算法解决此问题。
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function(nums) {
  let res = nums[0]
  let timeHigh = 1
  let timesMap = new Map()
  timesMap.set(nums[0], 1)
  const len = nums.length
  for(let i = 1; i < len; ++i) {
    if (timesMap.get(nums[i]) !== undefined) { // 出现过，累加
      timesMap.set(nums[i], timesMap.get(nums[i]) + 1)
    } else { // 没有出现过，设置为1
      timesMap.set(nums[i], 1)
    }
    if (timesMap.get(nums[i]) > timeHigh) {
      timeHigh = timesMap.get(nums[i])
      res = nums[i]
    }
  }
  return res
};

const demo1 = [3,2,3]
const res1 = majorityElement(demo1);
console.log(res1)