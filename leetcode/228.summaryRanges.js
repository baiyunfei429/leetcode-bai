/**
228. 汇总区间
给定一个  无重复元素 的 有序 整数数组 nums 。

返回 恰好覆盖数组中所有数字 的 最小有序 区间范围列表 。也就是说，nums 的每个元素都恰好被某个区间范围所覆盖，并且不存在属于某个范围但不属于 nums 的数字 x 。
列表中的每个区间范围 [a,b] 应该按如下格式输出：

"a->b" ，如果 a != b
"a" ，如果 a == b

示例 1：
输入：nums = [0,1,2,4,5,7]
输出：["0->2","4->5","7"]
解释：区间范围是：
[0,2] --> "0->2"
[4,5] --> "4->5"
[7,7] --> "7"

示例 2：
输入：nums = [0,2,3,4,6,8,9]
输出：["0","2->4","6","8->9"]
解释：区间范围是：
[0,0] --> "0"
[2,4] --> "2->4"
[6,6] --> "6"
[8,9] --> "8->9"
*/

var summaryRanges = function(nums) {
  let res = [];
  let startPoint = 0;
  let endPoint = 0;
  for (let i = 0; i < nums.length; i++) {
    if (nums[i+1] - nums[i] === 1) { // 说明是连续的，比如1/2/3，此时，endPoint向后移动一位
      endPoint = i + 1
    } else { // startPoint是开始数字的下标的，endPoint是结束数字下标 + 1
      if (startPoint === i) {
        res.push(`${nums[startPoint]}`)
      } else {
        res.push(`${nums[startPoint]}->${nums[i]}`)
      }
      startPoint = i + 1
    }
  }

  return res
};
const nums = [0,1,2,4,7,8]; // ["0->2","4->5","7"]
// const nums = [0,2,3,4,6,8,9]; // ["0","2->4","6","8->9"]
const result = summaryRanges(nums)
console.log(result)

