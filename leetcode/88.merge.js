/**
 * 88. 合并两个有序数组 【简单】
 * 给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组。
初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。你可以假设 nums1 的空间大小等于 m + n，这样它就有足够的空间保存来自 nums2 的元素。

示例 1：
输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
输出：[1,2,2,3,5,6]

示例 2：
输入：nums1 = [1], m = 1, nums2 = [], n = 0
输出：[1]
 
提示：
nums1.length == m + n
nums2.length == n
0 <= m, n <= 200
1 <= m + n <= 200
-109 <= nums1[i], nums2[i] <= 109

*/

/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function(nums1, m, nums2, n) {
  if (m === 0 || n === 0) {
    return m === 0 ? nums2 : nums1
  }
  let p1 = 0, p2 = 0
  while(p1 < m && p2 < n) {
    if (nums1[p1] <= nums2[p2]) {
      p1++
    } else if (nums1[p1] > nums2[p2]) {
      nums1.pop()
      nums1.splice(p1, 0, nums2[p2])
      p2++
      p1++
      m++
    }
  }
  if (p1 === m) {
    while(p2 < n) {
      nums1[m - 1 + p2] = nums2[p2]
      p2++
    }
  }
  return nums1
};

var merge2 = function(nums1, m, nums2, n) {
  let res = []
  let i = 0, j = 0, k = 0;
  while(i < m && j < n) {
    if (nums1[i] <= nums2[j]) {
      res[k++] = nums1[i++]
    } else {
      res[k++] = nums2[j++]
    }
  }
  for (; i < m;) {
    res[k++] = nums1[i++]
  }
  for (; j < n;) {
    res[k++] = nums2[j++]
  }
  for (let l = 0; l < m+n; l++) {
    nums1[l] = res[l]
  }
  return nums1
}

var merge3 = function(nums1, m, nums2, n) {
  let i = m - 1
  let j = n - 1
  let end = m + n - 1
  while (j >= 0) {
    nums1[end--] = (i >= 0 && nums1[i] > nums2[j]) ? nums1[i--] : nums2[j--];
  }
  return nums1
}

// const nums1 = [0], m = 0, nums2 = [1], n = 1
const nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
const res1 = merge3(nums1, m, nums2, n);
console.log(res1)