/**
 * 69. x 的平方根【简单】
 * 实现 int sqrt(int x) 函数。
计算并返回 x 的平方根，其中 x 是非负整数。
由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。

示例 1:
输入: 4
输出: 2

示例 2:
输入: 8
输出: 2

说明: 8 的平方根是 2.82842..., 
     由于返回类型是整数，小数部分将被舍去。
*/

/**
 * @param {number} x
 * @return {number}
 */
var mySqrt = function(x) {
  // 特殊值判断
  if (x === 0) {
    return 0;
  }
  if (x === 1) {
    return 1;
  }
  let start = 0, end = x, res = -1;
  while(start <= end) {
    let middle = Math.round((start + end)/2)
    const middleTimesSelf = middle * middle
    if (middleTimesSelf <= x) {
      res = middle
      start = middle + 1
    } else {
      end = middle - 1
    }
  }
  return res
};

const demo1 = 8
const res1 = mySqrt(demo1);
console.log(res1)