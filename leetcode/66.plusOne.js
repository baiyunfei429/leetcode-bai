/**
 * 66. 加一【简单】
 * 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。

最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
你可以假设除了整数 0 之外，这个整数不会以零开头。

示例 1：
输入：digits = [1,2,3]
输出：[1,2,4]
解释：输入数组表示数字 123。

示例 2：
输入：digits = [4,3,2,1]
输出：[4,3,2,2]
解释：输入数组表示数字 4321。

示例 3：
输入：digits = [0]
输出：[1]

提示：
1 <= digits.length <= 100
0 <= digits[i] <= 9
*/

/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function(digits) {
  if (digits[0] === 0) return [1]
  const len = digits.length
  let plusNum = 1 // 往上进位的数字，只能是0、1两个
  for(let i = len - 1; i >= 0; i--) {
    const sum = digits[i] + plusNum
    plusNum = sum > 9 ? 1 : 0
    digits[i] = sum % 10
  }
  if (plusNum === 1) { // 如果最后进位是1，那么unshift1到数组的0号位置
    digits.unshift(1)
  }
  return digits
};
const demo1 = [1,2,3]
const res1 = plusOne(demo1);
console.log(res1)