/**
 * 53. 最大子序和【简单】
 * 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

示例 1：
输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。

示例 2：
输入：nums = [1]
输出：1

示例 3：
输入：nums = [0]
输出：0

示例 4：
输入：nums = [-1]
输出：-1

示例 5：
输入：nums = [-100000]
输出：-100000

提示：
1 <= nums.length <= 3 * 104
-105 <= nums[i] <= 105

*/

/**
 * @param {number[]} nums
 * @return {number}
 */
/**
 * 思路：（此思路是有缺陷的，仅供参考，需要订正，代码里已经订正）
规则1：nums[0]设为初始值
规则2：遍历nums，第一个元素，第一个+第二个元素，第一个+第二个+第三个，以此类推，每次结果与res比较，>res则替换
规则3：第二个元素，第二个+第三个元素，第二个+第三个+第四个元素，以此类推，每次结果与res比较，>res则替换
 * */ 

var maxSubArray = function(nums) {
  const len = nums.length
  let res = nums[0]
  if (len === 1) return res
  for(let i = 0; i < len; i++) { // 外层循环控制从第几个元素开始，比如第1下标个开始的所有组合，即1，1、2，1、2、3，以此类推
    let resPosible = 0
    for(let j = i; j < len; j++) { // 内层循环控制着加几个元素，比如从1下标元素开始加1个，num[1];从3下标开始加3个，num[3]+num[4]+num[5]
      resPosible += nums[j]
      if (resPosible > res) res = resPosible
    }
  }
  return res
};


var maxSubArray2 = function(nums) {
  const len = nums.length
  let ans = nums[0], sum = 0
  for(let i = 0; i < len; i++) { 
      if(sum > 0) {
          sum += nums[i];
      } else {
          sum = nums[i];
      }
      ans = Math.max(ans, sum);
  }
  return ans;
}

var maxSubArray3 = function(nums) {
  const len = nums.length
  let pre = 0, maxAns = nums[0];
  for(let i = 0; i < len; i++) { 
      pre = Math.max(pre + nums[i], nums[i]);
      maxAns = Math.max(maxAns, pre);
  }
  return maxAns;
}


const demo1 = [-2,1]
const res1 = maxSubArray3(demo1);
console.log(res1)