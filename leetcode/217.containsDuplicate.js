/**
217. 存在重复元素
给你一个整数数组 nums 。如果任一值在数组中出现 至少两次 ，返回 true ；如果数组中每个元素互不相同，返回 false 。
 
示例 1：
输入：nums = [1,2,3,1]
输出：true

示例 2：
输入：nums = [1,2,3,4]
输出：false

示例 3：
输入：nums = [1,1,1,3,3,4,3,2,4,2]
输出：true
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function(nums) {
	if (!Array.isArray(nums)) throw new Error('请输入一个array')
	const map = new Map()
	const len = nums.length
	for (let i = 0; i < len; i += 1) {
		if (map.has(nums[i])) {
			return true;
		}
		map.set(nums[i], i)
	}
	return false;
};
var containsDuplicate2 = function(nums) {
	if (!Array.isArray(nums)) throw new Error('请输入一个array')
	const map = new Map()
	const len = nums.length
	for (let i = 0; i < len; i += 1) {
		if (map.has(nums[i])) {
			return true;
		}
		map.set(nums[i], i)
	}
	return false;
};
const nums = [1,1,1,3,3,4,3,2,4,2];
const result = containsDuplicate2(nums);
console.log(result);