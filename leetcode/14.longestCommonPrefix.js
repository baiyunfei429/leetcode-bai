/**
 * 14. 编写一个函数来查找字符串数组中的最长公共前缀。【简单】

如果不存在公共前缀，返回空字符串 ""。

示例 1：

输入：strs = ["flower","flow","flight"]
输出："fl"
示例 2：

输入：strs = ["dog","racecar","car"]
输出：""
解释：输入不存在公共前缀。

提示：
0 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] 仅由小写英文字母组成

*/

/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
  let res = ''
  const strsLen = strs.length,
  strs1 = strs[0],
  strs1Len = strs1.length;

  for (let i = 0; i < strs1Len; i++) {
    const resSplit = strs1[i]
    let flagIsCommonPrefix = true
    for(let j = 1; j < strsLen; j++) {
      if (strs[j][i] !== resSplit) {
        flagIsCommonPrefix = false
        return res;
      }
    }
    if (flagIsCommonPrefix) {
      res += resSplit
    }
  }
  return res
}


var longestCommonPrefix = function(strs) {
  if(strs.length < 1) return '';
  var [a, ...b] = strs;
  var result = '';
  for(let i = 0; i < a.length; i++) {
    var reg = new RegExp("^" + a.slice(0, i+1));
    var flag = b.every(item => reg.test(item));
    if(flag) result += a[i];
  }
  return result;
};

const demo1 = ["flower","flow","flight"]
const res1 = longestCommonPrefix(demo1)
console.log(res1)

const demo2 = ["dog","racecar","car"]
const res2 = longestCommonPrefix(demo2)
console.log(res2 === '')

const demo3 = ["flaower","flaow","flaiaght"]
const res3 = longestCommonPrefix(demo3)
console.log(res3)